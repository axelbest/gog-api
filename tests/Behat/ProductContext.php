<?php

use App\Entity\Product;
use Behat\Behat\Context\BehatContext;
use Behat\Gherkin\Node\TableNode;


class ProductContext extends BehatContext
{
    /**
     * @var Application
     */
    private static $app;


    /**
     * @Given /^the following products exist:$/
     */
    public function theFollowingProductExist(TableNode $table)
    {
        foreach ($table->getHash() as $row) {
            $id = $row['id'];
            unset($row['id']);

            $this->createProduct($id, null, $row);
        }
    }

    /**
     * @Given /^there is a programmer called "([^"]*)"$/
     */
    public function thereIsAProductCalled($name)
    {
        $this->createProduct($name);
    }


    public function createProduct($title, array $data = [])
    {

        $programmer = new Product();
        $programmer->setTitle($title);

        foreach ($data as $prop => $val) {
            $programmer->$prop = $val;
        }

        $this->getProgrammerRepository()->save($programmer);

        return $programmer;
    }


}