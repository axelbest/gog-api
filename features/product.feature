Feature: Product
  In order to be a gog dev and
  As an API client
  I need to be able to create products and handle them

  Background:
    Given the user "gog@gog.gog" exists

  Scenario: Create a product
    Given I have the payload:
    """
    {
      "title": "SampleTitle",
      "price": "6.78 USD"
    }
    """
    When I request "POST /api/products"
    Then the response status code should be 201
    And the "Location" header should not be empty


  Scenario: GET one product
    Given the following products exist:
      | id   | title    |
      | 1    | Fallout  |
    When I request "GET /api/products/1"
    Then the response status code should be 200
    And the following properties should exist:
    """
    id
    title
    price
    """

  Scenario: GET a collection of products
    Given the following products exist:
      | id    | title       |
      | 1     | Fallout     |
      | 6     | Bloodborne  |
    When I request "GET /api/products"
    Then the response status code should be 200
    And the "products" property should be an array
    And the "products" property should contain 2 items