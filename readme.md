GOG assignment
==============

## Table of Contents
1. [Requirements](#requirements)
1. [Installation](#installation)
1. [Usage](#usage)
	1. [Basics](#start)
    1. [Products catalog API](#products-api)
    1. [Cart API](#cart-api)

1. [Flaws](#flaws)
1. [Additonal parts](#additional_parts)

## Requirements
1. **PHP** -  needs to be a minimum version of PHP 7.1.3 (type hints, composer paackages, etc).
This assignment was made with PHP 7.2.21
2. **Composer**
This assignment was made with Composer version 1.10.15

3. **Symfony Framework** - needs to be a minimum version of Symfony Framework 4.4.*.
This assignment was made with SF 4.4.16 (LTS)

To see more specific requirements check the `composer.json` file.


## Installation
1. [Install Composer](https://getcomposer.org/download/).
2. Run `composer install` - at this point you might be asked to execute the behat recipe - select Y/Yes
3. Run `bin/console doctrine:schema:create`
4. Run `bin/console doctrine:fixtures:load`
5. Run `bin/console server:run`

## Usage

#### Start
All of the resources (excluding only the `/api/products -> GET method`) requires authorization

For the assignment purpose - we need to provide the token, which can be found in the database (user table -> `api_token` column) as well as in the DataFixture for user records.

To authorize as an admin (for adding products to the catalog), use the following token `gog_admin`. The fastest way is to create a CURL call like this:
`curl http://localhost:8000/api/products` - this will return the products catalog
for the protected resources use the following CURL call:
`curl -H "X-AUTH-TOKEN: gog_admin" http://localhost:8000/api/products`

For better visual data representation you might want to use the [Postman Application](https://www.postman.com)

#### Products API
Endpoints list:

`/api/products` **GET** --- Returns all products. No authorization required. Pagination not implemented yet.

`/api/products/{id}` **GET** --- Shows one record. No authorization required

`/api/products` **POST** --- Creates new product. Sample payload `{"title":"New game title", "price":"1.23 USD"}`

`/api/products/{id}` **DELETE** --- Removes product with specifici Id. Requires valid product id

`/api/products/{id}` **PATCH|PUT** --- Updates record. With the current implementation, those methods work the same way. The payload is the same as for the POST method


#### Cart API
Endpoints list:

`/api/cart` **POST** --- Creates new cart

`/api/cart` **DELETE** --- Removes the cart and all inner items

`/api/cart/product` **POST** --- Add product to cart. Sample payload `{"product_id":"4", "quantity":"2"}`

`/api/cart/product/{productId}` **DELETE** --- Deletes the specified product from cart 

`/api/cart` **GET** --- Returns all items placed in cart. Listing not implemented yet.

## Flaws
1. Missing pagination, especialy with the cursor approach.
2. PUT/PATCH not implemented completely/correctly (uniqueness validator is causing problems while updating)
3. Listing cart items not implemented.
4. Total price in cart not implemented.
5. Product price handling (validation, currency and price separation) not implemented
6. Behat tests missing, my sample behat tests made on silex couldn't be easily transferred to newer version of behat.
7. Unit tests missing.
8. Missing swagger documentation

... and lot's of places which can be refactored or written in better manner. 

## Additional Parts
1. Basic user authorization for API purpose
2. Event listener for handling custom API exceptions, not allowing to dispaly more than needed for end user
3. Few DataFixtures