<?php


namespace App\Controller\Api;


use App\Api\ApiProblem;
use App\Api\ApiProblemException;
use App\Form\CartItemType;
use App\Service\CartService;
use App\Service\ProductService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    use ApiHelper;

    /**
     * @Route("/api/cart", methods={"POST"})
     *
     * @param CartService $cartService
     *
     * @return Response
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createAction(CartService $cartService)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $user = $this->getUser();
        if ($cartService->isCartCreated($user->getId())) {
            $apiProblem = new ApiProblem(409, ApiProblem::TYPE_ALREADY_EXISTS_ERROR);
            $apiProblem->set("errors", ['body' => "Cart already exists"]);

            throw new ApiProblemException($apiProblem);
        }
        $cartService->createCart($user);

        return new Response(null, 201);
    }

    /**
     * @Route("/api/cart", methods={"DELETE"})
     *
     * @param CartService $cartService
     *
     * @return Response
     */
    public function deleteAction(CartService $cartService)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $userId = $this->getUser()->getId();

        $cartService->deleteCart($userId);

        return new Response(null, 204);
    }

    /**
     * @Route("/api/cart/product", methods={"POST"})
     *
     * @param Request $request
     * @param CartService $cartService
     * @param ProductService $productService
     *
     * @return JsonResponse
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addProductAction(Request $request, CartService $cartService, ProductService $productService)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $user = $this->getUser();
        $data = $this->handleInputData($request->getContent());

        if (!$cartService->isCartCreated($user->getId())) {
            $cartService->createCart($user);
        }

        $form = $this->createForm(CartItemType::class);
        $form->submit($data);

        if (!$form->isValid()) {
            $this->handleValidationError($form->getErrors(true));
        }

        $cart = $this->getDoctrine()->getRepository('App:Cart')->findOneBy(['user' => $user->getId()]);
        $product = $this->getDoctrine()->getRepository('App:Product')->find($data['product_id']);

        if (!$cartService->isAdditionAllowed($cart, $product)) {
            $apiProblem = new ApiProblem(400, ApiProblem::TYPE_VALIDATION_ERROR);
            $apiProblem->set("errors", ['cart_items_limit' => sprintf('You can\'t add more than %1$s cart items', $this->getParameter('app.cart_max_items'))]);

            throw new ApiProblemException($apiProblem);
        }

        $cartService->addProductToCart($cart, $product, $data['quantity']);

        return new JsonResponse(['status' => 'success'], 201);
    }

    /**
     * @Route("/api/cart/product/{productId}", methods={"DELETE"})
     */
    public function removeProductAction(CartService $cartService, int $productId)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $userId = $this->getUser()->getId();

        if (!$cartService->isCartCreated($userId)) {
            $apiProblem = new ApiProblem(400, ApiProblem::TYPE_VALIDATION_ERROR);
            $apiProblem->set("errors", ['cart' => "Nothing to remove. Cart doesn't exist"]);

            throw new ApiProblemException($apiProblem);
        }

        $cartId = $this->getUser()->getCart()->getId();

        $cartService->removeItemInCart($cartId, $productId);

        return new Response(null, 204);
    }


    /**
     * @Route("/api/cart", methods={"GET"})
     */
    public function listAction()
    {
        $products = $this->getDoctrine()->getRepository('App:Cart')->getCartItems();

        $data = ['cartProducts' => []];
        foreach ($products as $product) {
            $data['products'][] = $this->serialize($product);
        }

        return new JsonResponse($data, 200);
    }

}