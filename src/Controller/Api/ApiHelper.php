<?php


namespace App\Controller\Api;


use App\Api\ApiProblem;
use App\Api\ApiProblemException;
use Exception;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

trait ApiHelper
{
    /**
     * @param object $object
     * @param string $format
     * @return string
     */
    protected function serialize(object $object, string $format = 'json'): string
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);

        return $serializer->serialize($object, $format);
    }

    /**
     * @param string $inputData
     * @return array
     * @throws Exception
     */
    protected function handleInputData(string $inputData): array
    {
        $data = json_decode($inputData, true);
        if ($data === null) {
            $apiProblem = new ApiProblem(400, ApiProblem::TYPE_VALIDATION_ERROR);
            $apiProblem->set("errors", ['body' => "Invalid JSON data syntax"]);

            throw new ApiProblemException($apiProblem);
        }

        return $data;
    }

    /**
     * @param FormErrorIterator $formErrors
     * @throws Exception
     */
    protected function handleValidationError(FormErrorIterator $formErrors): void
    {
        $errorCollection = [];
        foreach ($formErrors as $error) {
            $errorCollection[$error->getOrigin()->getName()] = $error->getMessage();
        }

        $apiProblem = new ApiProblem(400, ApiProblem::TYPE_VALIDATION_ERROR);
        $apiProblem->set("errors", $errorCollection);

        throw new ApiProblemException($apiProblem);
    }

}