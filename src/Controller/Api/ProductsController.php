<?php


namespace App\Controller\Api;


use App\Api\ApiProblem;
use App\Api\ApiProblemException;
use App\Form\ProductType;
use App\Service\ProductService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


class ProductsController extends AbstractController
{
    use ApiHelper;

    /**
     * @Route("/api/products", methods={"POST"})
     *
     * @param Request $request
     * @param ProductService $productService
     *
     * @return JsonResponse
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createAction(Request $request, ProductService $productService)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN_USER');

        $data = $this->handleInputData($request->getContent());

        $form = $this->createForm(ProductType::class);
        $form->submit($data);

        if (!$form->isValid()) {
           $this->handleValidationError($form->getErrors(true));
        }

        $product = $productService->createProduct($data['title'], $data['price']);

        $url = $this->generateUrl('api_products_show', [
            'id' => $product->getId(),
        ]);

        $response = new JsonResponse($this->serialize($product), 201);
        $response->headers->set('Location', $url);

        return $response;
    }

    /**
     * @Route("/api/products/{id}", methods={"DELETE"})
     *
     * @param int $id
     * @return Response
     */
    public function deleteAction(int $id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN_USER');

        $productRepository = $this->getDoctrine()->getRepository('App:Product');

        $product = $productRepository->findOneBy(['id' => $id]);
        if ($product) {
            $productRepository->deleteById($id);

            return new Response(null, 204);
        }

        throw new NotFoundHttpException("Product was not found");
    }

    /**
     * @Route("/api/products", methods={"GET"})
     */
    public function listAction()
    {
        $products = $this->getDoctrine()->getRepository('App:Product')->findAll();

        $data = ['products' => []];
        foreach ($products as $product) {
            $data['products'][] = $this->serialize($product);
        }

        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/api/products/{id}", methods={"PATCH", "PUT"})
     *
     * @param Request $request
     * @param ProductService $productService
     * @param int $id Product ID
     *
     * @return Response
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAction(Request $request, ProductService $productService, int $id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN_USER');

        $product = $this->getDoctrine()->getRepository('App:Product')->findOneBy(['id' => $id]);
        if (!$product) {
            throw new NotFoundHttpException("Product was not found");
        }

        $data = $this->handleInputData($request->getContent());

        $form = $this->createForm(ProductType::class);
        $form->submit($data);
        if (!$form->isValid()) {
            $this->handleValidationError($form->getErrors(true));
        }

        $product = $productService->updateProduct($product, $data['title'], $data['price']);

        return new JsonResponse($this->serialize($product), 200);
    }

    /**
     * @Route("/api/products/{id}", name="api_products_show", methods={"GET"})
     *
     * @param int $id Product ID
     *
     * @return JsonResponse
     */
    public function showAction(int $id)
    {
        $product = $this->getDoctrine()->getRepository('App:Product')->findOneBy(['id' => $id]);
        if (!$product) {
            throw new NotFoundHttpException("Product was not found");
        }

        return new JsonResponse($this->serialize($product), 200);
    }
}