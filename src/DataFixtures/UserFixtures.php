<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setApiToken('gog_admin');
        $user->setEmail("gog@gog.gog");
        $user->setRoles(['ROLE_ADMIN_USER']);
        $user->setPassword($this->getCommonPassword($user));
        $manager->persist($user);

        for ($i = 0; $i < 5; $i++) {
            $customer = new User();
            $customer->setApiToken('test_user-' . $i);
            $customer->setEmail("test_user_email+" . $i . "@gog.com");
            $customer->setRoles(['ROLE_USER']);
            $customer->setPassword($this->getCommonPassword($customer));
            $manager->persist($customer);
        }

        $manager->flush();
    }

    private function getCommonPassword($user)
    {
        return $this->passwordEncoder->encodePassword(
            $user,
            'the_new_password'
        );
    }
}
