<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    private function getCatalogProducts()
    {
        return [
            ['title' => "Fallout", 'price' => "1.99 USD"],
            ['title' => "Don't Starve", 'price' => "2.99 USD"],
            ['title' => "Baldur's Gate", 'price' => "3.99 USD"],
            ['title' => "Icewind Dale", 'price' => "4.99 USD"],
            ['title' => "Bloodborne", 'price' => "5.99 USD"],
        ];
    }

    public function load(ObjectManager $manager)
    {
        $titles = $this->getCatalogProducts();
        foreach ($titles as $item) {
            $product = new Product();
            $product->setTitle($item['title']);
            $product->setPrice($item['price']);
            $manager->persist($product);
        }

        $manager->flush();
    }
}
