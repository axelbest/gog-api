<?php

namespace App\EventListener;

use App\Api\ApiProblem;
use App\Api\ApiProblemException;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Throwable;

class ExceptionListener
{
    private $kernel;

    /**
     * ExceptionListener constructor.
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @param ExceptionEvent $event
     * @throws Exception
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $request = $event->getRequest();
        $exception = $event->getThrowable();

        $isDebug = $this->kernel->isDebug();
        $statusCode = $this->getStatusCode($exception);

        if (!$this->isApiRequest($request)) {
            return;
        }

        if ($isDebug && $statusCode == 500) {
            return;
        }

        if ($exception instanceof ApiProblemException) {
            $apiProblem = $exception->getApiProblem();
        } else {
            $apiProblem = new ApiProblem($statusCode);
            if ($exception instanceof HttpException) {
                $apiProblem->set('detail', $exception->getMessage());
            }
        }

        $response = new JsonResponse(
            $apiProblem->toArray(),
            $apiProblem->getStatusCode()
        );
        $response->headers->set("Content-Type", "application/problem+json");

        $event->setResponse($response);
    }

    /**
     * @param Throwable $exception
     *
     * @return int
     */
    private function getStatusCode(Throwable $exception): int
    {
        $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;

        switch (true) {
            case $exception instanceof AuthenticationException:
                $statusCode = Response::HTTP_UNAUTHORIZED;

                break;
            case $exception instanceof InvalidArgumentException:
            case $exception instanceof InvalidParameterException:
                $statusCode = Response::HTTP_BAD_REQUEST;

                break;
            case $exception instanceof NotFoundHttpException:
                $statusCode = Response::HTTP_NOT_FOUND;

                break;
            case $exception instanceof AccessDeniedException:
                $statusCode = Response::HTTP_FORBIDDEN;

                break;
            case $exception instanceof HttpException:
                $statusCode = $exception->getStatusCode();

                break;
        }

        return $statusCode;
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function isApiRequest(Request $request): bool
    {
        return (strpos($request->getPathInfo(), '/api') === 0);
    }
}