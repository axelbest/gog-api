<?php

namespace App\Form;

use App\Validator\Constraints\isProductTitleInDatabase;
use App\Validator\Constraints\UniqueProductTitle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'Title',
            'constraints' => [
                new NotBlank(),
                new UniqueProductTitle(),
            ],
        ]);

        $builder->add('price', TextType::class, [
            'label' => 'Price',
            'constraints' => [
                new NotBlank(),
            ],
        ]);

        $builder->add('save', SubmitType::class);
    }

}