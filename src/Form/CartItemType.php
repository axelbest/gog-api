<?php

namespace App\Form;

use App\Validator\Constraints\ExistingProduct;
use App\Validator\Constraints\isProductTitleInDatabase;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;

class CartItemType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('product_id', IntegerType::class, [
            'label' => 'Product ID',
            'constraints' => [
                new NotBlank(),
                new ExistingProduct(),
            ],
        ]);

        $builder->add('quantity', RangeType::class, [
            'label' => 'Quantity',
            'attr' => [
                'min' => 1,
                'max' => 10,
            ],
            'constraints' => [
                new NotBlank(),
                new GreaterThan(0),
                new LessThanOrEqual(10),
            ],
        ]);

        $builder->add('save', SubmitType::class);
    }
}