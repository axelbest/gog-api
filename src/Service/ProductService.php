<?php


namespace App\Service;


use App\Entity\Product;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class ProductService
{

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct($entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param string $title
     * @param string $price
     * @return null
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createProduct(string $title, string $price)
    {
        $product = new Product();
        $product->setTitle($title);
        $product->setPrice($price);

        $this->em->persist($product);
        $this->em->flush();

        return $product;
    }


    public function deleteProduct($id)
    {
        $this->em->getRepository("App:Product")->findOneBy();
    }

    /**
     * @param Product $product
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateProduct(Product $product, string $title, string $price)
    {
        $product->setTitle($title);
        $product->setPrice($price);

        $this->em->flush();

        return $product;
    }

    /**
     * @param int $productId
     * @return bool
     */
    public function productExists(int $productId): bool
    {
        $repository = $this->em->getRepository("App:Product");
        $product = $repository->findOneBy(['id' => $productId]);

        return !empty($product);
    }


}