<?php


namespace App\Service;


use App\Api\ApiProblem;
use App\Api\ApiProblemException;
use App\Entity\Cart;
use App\Entity\CartProduct;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CartService
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var int
     */
    private $maxItemQuantity;

    /**
     * @var int
     */
    private $maxItems;

    public function __construct($entityManager, int $itemsQuantityLimit, int $itemsLimit)
    {
        $this->em = $entityManager;
        $this->maxItemQuantity = $itemsQuantityLimit;
        $this->maxItems = $itemsLimit;
    }

    /**
     * @param User $user
     * @return Cart
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createCart(User $user)
    {
        $cart = new Cart();
        $cart->setUser($user);
        $this->em->persist($cart);
        $this->em->flush();

        return $cart;
    }


    public function deleteCart(int $userId)
    {
        if ($this->isCartCreated($userId)) {

            $cart = $this->em->getRepository('App:Cart')->findOneBy(['user' => $userId]);
            $this->em->remove($cart);
            $this->em->flush();

            return null;
        }

        throw new NotFoundHttpException("Cart was not found");
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function isCartCreated(int $userId): bool
    {
        $repository = $this->em->getRepository("App:Cart");
        $cart = $repository->findOneBy(['user' => $userId]);

        return !empty($cart);
    }

    public function isAdditionAllowed(Cart $cart, Product $product)
    {
        $isAdditionAllowed = false;

        $cartItems = $cart->getItems();
        $cartItemsIdList = [];
        foreach ($cartItems as $item) {
            array_push($cartItemsIdList, $item->getProduct()->getId());
        }

        if (count($cartItemsIdList) < $this->maxItems) {
            return true;
        }

        if (count($cartItemsIdList) === $this->maxItems && in_array($product->getId(), $cartItemsIdList)) {
            return true;
        }

        return $isAdditionAllowed;
    }

    public function addProductToCart(Cart $cart, Product $product, int $quantity)
    {
        /** @var CartProduct $existingRow */
        $existingRow = $this->em->getRepository("App:CartProduct")->findOneBy(['cart' => $cart->getId(), 'product' => $product->getId()]);

        if ($existingRow) {
            $updatedQuantity = $this->handleQuantity($existingRow->getQuantity(), $quantity);
            $existingRow->setQuantity($updatedQuantity);
        } else {
            $cartProduct = new CartProduct();
            $cartProduct->setProduct($product);
            $cartProduct->setCart($cart);
            $cartProduct->setQuantity($quantity);

            $this->em->persist($cartProduct);
        }

        $this->em->flush();
    }

    public function getCartItems()
    {

    }

    public function removeItemInCart(int $cartId, int $productId)
    {
        $cartItem = $this->em->getRepository("App:CartProduct")->findOneBy(['cart' => $cartId, 'product' => $productId]);
        if (!$cartItem) {
            $apiProblem = new ApiProblem(400, ApiProblem::TYPE_NOT_EXISTS);
            $apiProblem->set("errors", ['body' => "No such item in cart"]);

            throw new ApiProblemException($apiProblem);
        }

        $this->em->remove($cartItem);
        $this->em->flush();

    }

    /**
     * @param int $currentQuantity
     * @param int $addedQuantity
     * @return int
     */
    private function handleQuantity(int $currentQuantity, int $addedQuantity): int
    {
        $quantitySum = $currentQuantity + $addedQuantity;

        return ($quantitySum > $this->maxItemQuantity) ? $this->maxItemQuantity : $quantitySum;
    }

}