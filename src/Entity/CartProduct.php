<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cart_product")
 */
class CartProduct
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cart", inversedBy="cartProducts")
     * @ORM\JoinColumn(nullable=false)
     * @ORM\Id
     *
     * @var Cart
     */
    private $cart;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="cartItems")
     * @ORM\JoinColumn(nullable=false)
     * @ORM\Id
     *
     * @var Product
     */
    private $product;


    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     *
     * @var integer
     */
    private $quantity;

    public function getCart(): Cart
    {
        return $this->cart;
    }

    public function setCart(Cart $cart): void
    {
        $this->cart = $cart;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }
}