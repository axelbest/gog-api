<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class ExistingProduct extends Constraint
{
    public $message = "Product doesn't exists";

    public function validatedBy()
    {
        return ExistingProductValidator::class;
    }
}