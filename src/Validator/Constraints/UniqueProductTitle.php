<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class UniqueProductTitle extends Constraint
{
    public $message = "Title already exists";

    public function validatedBy()
    {
        return UniqueProductTitleValidator::class;
    }
}