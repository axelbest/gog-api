<?php


namespace App\Api;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class ApiProblem
{
    const TYPE_ALREADY_EXISTS_ERROR = "already_exists_error";
    const TYPE_NOT_EXISTS = "not_exists";
    const TYPE_VALIDATION_ERROR = "validation_error";
    const TYPE_INVALID_REQUEST_BODY_FORMAT = "invalid_body_format";
    const TYPE_LIMIT_EXCEEDED = "limit_exceeded";

    private static $titles = [
        self::TYPE_ALREADY_EXISTS_ERROR => "Item already exists",
        self::TYPE_VALIDATION_ERROR => "There was a validation error",
        self::TYPE_INVALID_REQUEST_BODY_FORMAT => "Invalid JSON format",
        self::TYPE_LIMIT_EXCEEDED => "Limit exceeded",
        self::TYPE_NOT_EXISTS => "Item doesn't exists",
    ];

    private $statusCode;
    private $type;
    private $title;
    private $extraData = [];

    /**
     * ApiProblem constructor.
     * @param int $statusCode
     * @param null $type
     * @throws Exception
     */
    public function __construct(int $statusCode, $type = null)
    {
        $this->statusCode = $statusCode;
        $this->type = $type;

        if ($type === null) {
            $this->type = 'about:blank';
            $this->title = isset(Response::$statusTexts[$statusCode]) ? Response::$statusTexts[$statusCode] : 'Unknown status code :(';
        } else {
            if (!isset(self::$titles[$type])) {
                throw new Exception(sprintf('No title for type "%s"', $type));
            }
            $this->title = self::$titles[$type];
        }
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $name
     * @param array $data
     */
    public function set(string $name, array $data): void
    {
        $this->extraData[$name] = $data;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return array_merge(
            [
                'title' => $this->title,
                'type' => $this->type,
                'status' => $this->statusCode,
            ],
            $this->extraData
        );
    }
}